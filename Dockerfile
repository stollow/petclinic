FROM maven:3.6.1-jdk-13 AS build
WORKDIR /home/app
COPY pom.xml .
RUN mvn dependency:go-offline
COPY . /home/app
RUN mvn spring-javaformat:apply
RUN  mvn package -DskipTests

FROM java
COPY --from=build /home/app/target/*.jar .
EXPOSE 8080
CMD java -jar *.jar